file(REMOVE_RECURSE
  "CMakeFiles/CppRaytracer.dir/CppRaytracer.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Scene/Entity.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Scene/Light.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Scene/PerspectiveCamera.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Scene/Scene.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Shading/ColorNode.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Shading/DiffuseNode.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Shading/ShaderNode.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Shape/AxisAlignedBox.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Shape/Cone.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Shape/PointShape.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Shape/Quad.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Shape/Sphere.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Shape/Triangle.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Tests.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Toolbox/Color.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Toolbox/ImageBuffer.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Toolbox/Matrix.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Toolbox/Point.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Toolbox/Vector.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Toolbox/lodepng.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Toolbox/toolbox.cpp.obj"
  "CMakeFiles/CppRaytracer.dir/Toolbox/transformation.cpp.obj"
  "CppRaytracer.exe"
  "CppRaytracer.exe.manifest"
  "CppRaytracer.pdb"
  "libCppRaytracer.dll.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/CppRaytracer.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
