//
// Created by arnor on 8/12/2020.
//

#include "PerspectiveCamera.hpp"

Ray PerspectiveCamera::generateRay(double x, double y){
    double u = width * (x * invxResolution - 0.5);
    double v = height * (y * invyResolution - 0.5);

    Vector direction = (basis->get_u() * u) + (basis->get_v() * v) - basis->get_w();

    return Ray(*origin, direction);
}