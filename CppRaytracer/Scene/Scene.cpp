//
// Created by arnor on 8/12/2020.
//

#include <iostream>
#include "Scene.hpp"
#include "Entity.hpp"
#include "../Shape/Quad.hpp"

bool Scene::findIntersection(Ray *r, size_t tMin, Intersection*& intersection) const {

    bool result = false;

    for(Entity* entity: entities){
        Intersection* t = nullptr;
        bool doesIntersect = entity->testIntersection(r, tMin, t); // tMin maybe as ref so shapes can do this themselves?
        if(doesIntersect){
            if(t->getT() < tMin){
                intersection = t;
                tMin = t->getT();
                result = true;
            }
        }
    }
    return result;
}

void Scene::add_entity(Entity* e) {
    entities.push_back(e);
}

void Scene::add_entity(Light* e) {
    entities.push_back(e);
    lights.push_back(e);
}

bool Scene::testVisibility(const Entity *e1, const Point *p1, Entity *e2, const Point *p2,
                           const Vector *normalP1, Intersection*& intersection) const {
    Vector p1p2 = *p2 - *p1;

    Ray shadowRay(*p1, p1p2.normalize()); // Create shadow ray from P1 -> towards P2

    double  startingCosAngle = p1p2.dot(*normalP1) / (p1p2.length() * normalP1->length());
    if(startingCosAngle < 0) return false;

    Intersection* i = nullptr;
    bool doesIntersect = findIntersection(&shadowRay, i);
    if(!doesIntersect){
        // No point was found, not even e2. Therefore no Intersection is made and added to the heap
        Vector p2p1 = *p1 - *p2;
        intersection = new Intersection(p2p1.length(), p2p1.normalize(), shadowRay); // create fake intersection
        intersection->set_entity(const_cast<Entity *>(e2));
        return true; // This should only happen due to numerical errors
        // and could happen with very small objects, like pointShapes.
        // Only because of this we can fake the intersection
    }
    //else if(intersection->get_entity() == e1) return false; // Closest intersection was with e1, itself
    if(i->get_entity() == e2){
        intersection = i;
        return true; // Closest intersection was with e2, the other
    }
    else if(i->getT() >= p1p2.length()){
        intersection = i;
        return true; // Another point was found behind p2
    }
    delete i; // remove the intersection found in between e1 and e2 on the heap
    return false;
}

bool Scene::findIntersection(Ray* r, Intersection*& intersection) const {
    return findIntersection(r, maximumRaydistance, intersection);
}
