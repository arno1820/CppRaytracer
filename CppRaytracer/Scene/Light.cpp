
#include "Light.hpp"
#include "../Shape/Shape.hpp"
#include "../Shading/ColorNode.hpp"

Light::Light(Shape *s, Color c) : Entity(s, new ColorNode(c)) {}

std::vector <Point> Light::getPointsOnLight() {
    return {1,getShape()->getPosition()}; // Currently only a point light, so just the position needs to be returned
}


