#pragma once
#include "../Toolbox/Point.hpp"
#include "../Toolbox/Color.hpp"
#include "../Toolbox/Ray.hpp"

class Intersection;
class ShaderNode;
class Shape;
class Scene;

class Entity
{
private:
    Shape* shape;
    ShaderNode* shaderPipeline;
public:
    Entity(Shape* s, ShaderNode* sp): shape{ s }, shaderPipeline{sp} {};
    ~Entity();
    bool testIntersection(Ray* r, double tMin, Intersection*& intersection);
    Shape* getShape() const { return shape; };
    Color evaluateRenderEquation(Intersection* i, Scene* s, int depth) const ;

    // For debugging
    //Color evaluateDirectLightNormalsVisibility(Intersection* i, Scene* s);
    //Color evaluateNormals(Intersection* i);
    //Color evaluateIntersection() { return Color(1, 1, 1); };
};
