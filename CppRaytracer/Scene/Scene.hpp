//
// Created by arnor on 8/12/2020.
//

#ifndef CPPRAYTRACER_SCENE_HPP
#define CPPRAYTRACER_SCENE_HPP
#include <utility>
#include <vector>
#include "../Toolbox/Intersection.hpp"
#include "Entity.hpp"
#include "Light.hpp"

class PerspectiveCamera;

class Scene {
private:
    std::vector<Entity*> entities{};
    std::vector<Light*> lights;
    PerspectiveCamera* camera;
public:
    size_t maximumRaydistance = 10000;
    explicit Scene(PerspectiveCamera* c): camera{c} {};
    std::vector<Entity*> getEntities() const {return entities;}
    std::vector<Light*> getLights() const {return lights;}
    void add_entity(Entity* e);
    void add_entity(Light* e);
    PerspectiveCamera* getCamera() const {return camera;}
    bool findIntersection(Ray* r, size_t tMin, Intersection*& intersection) const;
    bool findIntersection(Ray* r, Intersection*& intersection) const;
    bool testVisibility(const Entity* e1, const Point* p1, Entity* e2, const Point* p2, const Vector* normalP1,
                        Intersection*& intersection) const;
};


#endif //CPPRAYTRACER_SCENE_HPP
