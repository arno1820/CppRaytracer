#pragma once
#include "Entity.hpp"
#include <vector>
class Light: public Entity {
public:
    // Currently every light is a point light
    Light(Shape *s, Color c);
    std::vector<Point> getPointsOnLight();

};