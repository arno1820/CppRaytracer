#include "Entity.hpp"
#include "../Toolbox/Intersection.hpp"
#include "Scene.hpp"
#include "../Shape/Shape.hpp"
#include "../Shading/ShaderNode.hpp"

Entity::~Entity(){
    delete shape;
    delete shaderPipeline;
}

Color Entity::evaluateRenderEquation(Intersection* i, Scene* s, int depth) const{
    return shaderPipeline->eval(i,s,depth,this);
}

bool Entity::testIntersection(Ray* r, double tMin, Intersection*& intersection){
    bool doesIntersect = shape->intersect(r, tMin, intersection);
    if(doesIntersect){
        intersection->set_entity(this);
        return true;
    }
    return false;
}