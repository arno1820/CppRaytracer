//
// Created by arnor on 8/12/2020.
//

#ifndef CPPRAYTRACER_PERSPECTIVECAMERA_HPP
#define CPPRAYTRACER_PERSPECTIVECAMERA_HPP
#include "../Toolbox/toolbox.hpp"
#include "../Toolbox/Point.hpp"
#include "../Toolbox/OrthonormalBasis.hpp"
#include "../Toolbox/Ray.hpp"
#include "Scene.hpp"
#include "../Toolbox/ImageBuffer.hpp"

class PerspectiveCamera {
private:
    int tileDevision = 64;
    int antiAiliasing = 16;
    Point* origin = new Point(0,0,0);
    OrthonormalBasis* basis = new OrthonormalBasis(Vector(0,0,1).invert(), Vector(0,1,0));
    double width = 200;
    double height = 200;
    double invxResolution;
    double invyResolution;
public:
    PerspectiveCamera(int xResolution, int yResolution, Point* pos, Vector lookat, Vector up, double fov){
        if (xResolution < 1) throw "";
        if (yResolution < 1) throw "";
        if (fov <= 0) throw "";
        if (fov > 180) throw "";
        origin = pos;
        basis = new OrthonormalBasis(lookat.invert(), up);

        invxResolution = 1 / (double) xResolution;
        invyResolution = 1 / (double) yResolution;
        width = 2 * tan(0.5 * toRads(fov));
        height = yResolution * width * invxResolution;
    }
    ~PerspectiveCamera(){
        delete origin;
        delete basis;
    }
    Ray generateRay(double x, double y);

    //TODO: debug option generateBVHMapFrame


};


#endif //CPPRAYTRACER_PERSPECTIVECAMERA_HPP
