//
// Created by arnor on 4/12/2020.
//

#include "Sphere.hpp"
#include "../Toolbox/Intersection.hpp"
#include <cmath>

bool Sphere::intersect(Ray* r, double tMin, Intersection*& intersection){

    Ray transformed = transformation.transformInverse(*r);

    Vector o = transformed.get_origin().toVector();

    double a = transformed.get_direction().lengthSquared();
    double b = 2 * (transformed.get_direction().dot(o));
    double c = o.dot(o) - 1;

    double d = b * b - 4 * a * c;

    if(d < 0) return false;

    double dr = sqrt(d);

    // numerically solve the equation a*t^2 + b * t + c = 0
    double q = -0.5 * (b < 0 ? (b - dr) : (b + dr));

    double t0 = q / a;
    double t1 = c / q;

    if(t0 < 0 && t1 < 0) return false;

    // smallest t gives closest intersection
    double tclosest = t0;
    if(t1 < t0 || tclosest < 0) tclosest = t1;

    if(tclosest > tMin) return false;

    intersection = new Intersection(tclosest, calculateNormal(r->get_origin() + (r->get_direction().normalize() * tclosest)), *r);

    return true;
}

Vector Sphere::calculateNormal(Point p) {
    Vector unitNormal = transformation.transformInverse(p).toVector();

    // Calculate world normal
    return transformation.transformNormal(unitNormal);
}
