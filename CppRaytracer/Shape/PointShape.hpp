//
// Created by arnor on 8/12/2020.
//


#ifndef CPPRAYTRACER_POINTSHAPE_HPP
#define CPPRAYTRACER_POINTSHAPE_HPP

#include "Shape.hpp"

class PointShape: public Shape {
public:
    PointShape(Transformation t) : Shape(t) {};
    bool intersect(Ray* r, double tMin, Intersection*& intersection) override;
};


#endif //CPPRAYTRACER_POINTSHAPE_HPP
