//
// Created by arnor on 22/12/2020.
//

#ifndef CPPRAYTRACER_AXISALIGNEDBOX_HPP
#define CPPRAYTRACER_AXISALIGNEDBOX_HPP


#include "Shape.hpp"

class AxisAlignedBox: public Shape {
private:
    Point p1;
    Point p2;
    static const double kEpsilon;
public:
    AxisAlignedBox(Point point_1, Point point_2): Shape(Transformation()), p1{point_1}, p2{point_2} {
        // P1 as the lowest and P2 as highest
        double lowest_x = point_1.x(), lowest_y = point_1.y(), lowest_z = point_1.z();
        double highest_x = point_2.x(), highest_y = point_2.y(), highest_z = point_2.z();
        if(lowest_x > highest_x) {
            lowest_x = highest_x;
            highest_x = point_1.x();
        }
        if(lowest_y > highest_y) {
            lowest_y = highest_y;
            highest_y = point_1.y();
        }
        if(lowest_z > highest_z) {
            lowest_z = highest_z;
            highest_z = point_1.z();
        }
        p1 = {lowest_x, lowest_y, lowest_z};
        p2 = {highest_x, highest_y, highest_z};
    };
    /**
	 * Makes new AxisAlignedBox, originally a cube with center at (0,0,0) and sides of 1x1
	 * @param position Position of this AAB in the world
	 * @param xSize the x-Scale of this AAB
	 * @param ySize the y-Scale of this AAB
	 * @param zSize the z-Scale of this AAB
	 */
    AxisAlignedBox(Point position, double xSize, double ySize, double zSize) :
            Shape(Transformation()),
            p1{position.x() - (xSize / 2), position.y() - (ySize / 2), position.z() - (zSize / 2)},
            p2{position.x() + (xSize / 2), position.y() + (ySize / 2), position.z() + (zSize / 2)} {};
    ~AxisAlignedBox() override = default;

    bool intersect(Ray* r, double tMin, Intersection*& intersection) override;

};


#endif //CPPRAYTRACER_AXISALIGNEDBOX_HPP
