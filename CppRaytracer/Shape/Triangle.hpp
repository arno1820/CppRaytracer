//
// Created by arnor on 22/12/2020.
//

#ifndef CPPRAYTRACER_TRIANGLE_HPP
#define CPPRAYTRACER_TRIANGLE_HPP

#include "Shape.hpp"

class Triangle: public Shape {
private:
    Point A;
    Vector na;
    Point B;
    Vector nb;
    Point C;
    Vector nc;
public:
    /**
     * Creates a triangle from vertexA, vertexB and vertexC with a normal in direction of the vector vertexA->vertexB X
     * vertexA->vertexC.
     * t transforms this triangle.
     */
    Triangle(Transformation t, Point vertexA, Point vertexB, Point vertexC): Shape(t), A{vertexA}, B{vertexB},
    C{vertexC}, na{(vertexB - vertexA).cross(vertexC - vertexA).normalize()}, nb{na}, nc{nc} {};
    /**
     * Creates a triangle from vertexA, vertexB and vertexC with the given normals.
     * t transforms this triangle.
     */
    Triangle(Transformation t,
                      Point vertexA, Vector normalA, Point vertexB, Vector normalB, Point vertexC, Vector normalC):
                      Shape(t), A{vertexA}, B{vertexB}, C{vertexC}, na{normalA}, nb{normalB}, nc{normalC} {};
    ~Triangle() override = default;

    bool intersect(Ray* r, double tMin, Intersection*& intersection) override;
};


#endif //CPPRAYTRACER_TRIANGLE_HPP
