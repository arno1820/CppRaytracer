#ifndef CPPRAYTRACER_SHAPE_H
#define CPPRAYTRACER_SHAPE_H
#include "../Toolbox/Point.hpp"
#include "../Toolbox/Transformation.hpp"

class Intersection;

class Shape {
protected:
    Transformation transformation;
public:
    Shape(Transformation t): transformation{t} {};
    virtual ~Shape() = default;
    virtual Point getPosition() const {return transformation.transform(Point(0,0,0));};
    virtual bool intersect(Ray* r, double tMin, Intersection*& intersection) = 0;
};

#endif //CPPRAYTRACER_SHAPE_H
