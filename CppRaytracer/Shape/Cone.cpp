//
// Created by arnor on 22/12/2020.
//

#include "Cone.hpp"
#include "../Toolbox/Intersection.hpp"
#include <cmath>

bool Cone::intersect(Ray *ray, double tMin, Intersection *&intersection) {

    Ray inversed = transformation.transformInverse(*ray);

    Vector dir = inversed.get_direction();
    Point o = inversed.get_origin();

    double hhrr = (height*height)/(radius*radius);

    double a = -(dir.y() * dir.y()) + hhrr * (dir.x() * dir.x() + dir.z() * dir.z()); //Double checked
    double b = (- 2 * o.y() * dir.y() + 2 * dir.y() * height) + 2 * hhrr * (o.x()*dir.x() + o.z() * dir.z()); //Double checked
    double c = (- o.y() * o.y() + 2* o.y() * height - height*height) + hhrr * (o.x()*o.x() + o.z()*o.z()); //Double checked

    double D = b*b - 4 * a * c;

    if(D < 0){
        return false;
    }
    if(D == 0) {

        double t = -b/ (2*a);

        double y = o.y() + t*dir.y();
        if(y<0 || y>height) return false;
        intersection = new Intersection(t, calculateNormal(ray->get_origin() + ray->get_direction() * t), *ray);
        return true;
    }
    else if (D > 0){

        double t1 = (-b - sqrt(D))/(2*a);
        double t2 = (-b+ sqrt(D))/(2*a);

        double y1 = o.y() + t1*dir.y();
        double y2 = o.y() + t2*dir.y();

        bool considerT1 = false;
        bool considerT2 = false;

        if(t1 > 0 && (y1 > 0 && y1 < height) && t1 < tMin) considerT1 = true;
        if(t2 > 0 && (y2 > 0 && y2 < height) && t2 < tMin) considerT2 = true;

        if(considerT1 && considerT2){
            if(t1 < t2) intersection = new Intersection(t1, calculateNormal(ray->get_origin() + ray->get_direction() * t1),
                                                *ray);
            else intersection = new Intersection(t2, calculateNormal(ray->get_origin() + ray->get_direction() * t2),
                                                *ray);
            return true;
        }
        else if(considerT1){
            intersection = new Intersection(t1, calculateNormal(ray->get_origin() + ray->get_direction() * t1), *ray);
            return true;
        }
        else if(considerT2){
            intersection = new Intersection(t2, calculateNormal(ray->get_origin() + ray->get_direction() * t2), *ray);
            return true;
        }

    }
    return false;

}

Vector Cone::calculateNormal(Point intersection_p) {
    Point i = transformation.transformInverse(intersection_p);
    Vector normal{(2*height*i.x())/radius, -2*(i.y()-height),(2*height*i.z())/radius};
    return transformation.transformNormal(normal.normalize());
}
