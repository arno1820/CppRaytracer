//
// Created by arnor on 22/12/2020.
//

#ifndef CPPRAYTRACER_QUAD_HPP
#define CPPRAYTRACER_QUAD_HPP


#include "Shape.hpp"
class Quad: public Shape {
public:
    /**
     * Creates a quad from (-0.5,0.5,0) to (0.5,-0.5,0) and normal (0,0,-1).
     * @param t transforms this basic quad.
     */
    explicit Quad(Transformation t): Shape(t) {};
    ~Quad() override = default;

    bool intersect(Ray* r, double tMin, Intersection*& intersection) override;
};


#endif //CPPRAYTRACER_QUAD_HPP
