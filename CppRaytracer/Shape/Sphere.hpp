//
// Created by arnor on 4/12/2020.
//

#ifndef CPPRAYTRACER_SPHERE_H
#define CPPRAYTRACER_SPHERE_H

#include <utility>

#include "Shape.hpp"
#include "../Toolbox/Transformation.hpp"

class Sphere : public Shape {
private:
    Vector calculateNormal(Point p);

public:
    explicit Sphere(Transformation t): Shape(t) {};
    ~Sphere() override = default;

    bool intersect(Ray* r, double tMin, Intersection*& intersection) override;
};


#endif //CPPRAYTRACER_SPHERE_H
