//
// Created by arnor on 22/12/2020.
//

#include "Triangle.hpp"
#include "../Toolbox/Intersection.hpp"

bool Triangle::intersect(Ray *ray, double tMin, Intersection *&intersection) {

    Ray inverted_r = transformation.transformInverse(*ray);

    //          A - B       ;           A - C          ;             Ray dir                      ;            A - RayOrigin
    double a = A.x() - B.x(); double b = A.x() - C.x() ; double c = inverted_r.get_direction().x(); double d = A.x() - inverted_r.get_origin().x();
    double e = A.y() - B.y(); double f = A.y() - C.y() ; double g = inverted_r.get_direction().y(); double h = A.y() - inverted_r.get_origin().y();
    double i = A.z() - B.z(); double j = A.z() - C.z() ; double k = inverted_r.get_direction().z(); double l = A.z() - inverted_r.get_origin().z();

    double m = f*k-g*j; double n = h*k - g*l; double p = f*l - h*j;
    double q = g*i - e*k; double s = e*j - f*i;

    double inv_denom = 1/(a*m + b*q + c*s);

    double e1 = d*m - b*n - c*p;
    double beta = e1*inv_denom;

    if(beta < 0) return false;

    double r = e*l - h*i;
    double e2 = a*n + d*q + c*r;
    double gamma = e2 * inv_denom;

    if(gamma < 0) return false;
    if(beta+gamma > 1) return false;

    double e3 = a*p - b*r + d*s;

    if(e3*inv_denom > tMin || e3*inv_denom < 0) return false;

    // Calculate the normal
    double alpha = 1-beta-gamma;
    Vector normal{na.x()*alpha + nb.x()*beta + nc.x()*gamma,
                  na.y()*alpha + nb.y()*beta + nc.y()*gamma,
                  na.z()*alpha + nb.z()*beta + nc.z()*gamma};
    normal = normal.normalize();
    normal = transformation.transformNormal(normal);

    intersection = new Intersection(e3 * inv_denom,normal, *ray);

    return true;
}

