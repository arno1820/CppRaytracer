//
// Created by arnor on 22/12/2020.
//

#include "Quad.hpp"
#include "../Toolbox/Intersection.hpp"


bool Quad::intersect(Ray *r, double tMin, Intersection *&intersection) {

    Ray local_r = transformation.transformInverse(*r);

    double t = -local_r.get_origin().z()/local_r.get_direction().z(); // calculate if local ray intersects with the Z plane
    if(t < tMin && t > 0) {
        Point p = local_r.get_origin() + local_r.get_direction() * t;
        if (p.x() > -0.5 && p.x() < 0.5 && p.y() > -0.5 && p.y() < 0.5) { // Does the point lie in the quad?
            Vector normal(0,0,1);
            normal = transformation.transform(normal); // transform normal to world
            intersection = new Intersection(t, normal, *r);
            return true;
        }
    }
    return false;
}
