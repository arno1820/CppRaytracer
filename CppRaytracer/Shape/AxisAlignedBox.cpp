//
// Created by Arno Rondou on 22/12/2020.
// Code based on the book 'Raytracing from the ground up' Written by Kevin Suffern.
//

#include "AxisAlignedBox.hpp"
#include "../Toolbox/Intersection.hpp"

const double AxisAlignedBox::kEpsilon = 0.001;

Vector calculateNormal(const int face_hit){
    switch (face_hit) {
        case 0: return Vector(-1,0,0); // -x face
        case 1: return Vector(0,-1,0); // -y face
        case 2: return Vector(0,0,-1); // -z face
        case 3: return Vector(1,0,0); // x face
        case 4: return Vector(0,1,0); // y face
        case 5: return Vector(0,0,1); // z face
    }
    throw "Face needs to be a int of range [0,5]!";
}

bool AxisAlignedBox::intersect(Ray *ray, double tMin, Intersection *&intersection) {
    Vector dir = ray->get_direction();
    Point o = ray->get_origin();

    double tx_min, ty_min, tz_min;
    double tx_max, ty_max, tz_max;

    //Bepaal het snijpunt met de Vlakken die de box definiëren
    //Snijding x-coordinaat
    double a = 1/dir.x();
    if(a >= 0){
        tx_min = (p1.x() - o.x()) * a;
        tx_max = (p2.x() - o.x()) * a;
    }
    else {
        tx_min = (p2.x() - o.x()) * a;
        tx_max = (p1.x() - o.x()) * a;
    }

    //Snijding y-coordinaat
    double b = 1/dir.y();
    if(b >= 0){
        ty_min = (p1.y() - o.y()) * b;
        ty_max = (p2.y() - o.y()) * b;
    }
    else {
        ty_min = (p2.y() - o.y()) * b;
        ty_max = (p1.y() - o.y()) * b;
    }

    //Snijding z-coordinaat
    double c = 1/dir.z();
    if(c >= 0){
        tz_min = (p1.z() - o.z()) * c;
        tz_max = (p2.z() - o.z()) * c;
    }
    else {
        tz_min = (p2.z() - o.z()) * c;
        tz_max = (p1.z() - o.z()) * c;
    }

    //Vind de grootste t (t0)
    double t0, t1;
    int face_in; int face_out;

    if(tx_min > ty_min){
        t0 = tx_min;
        face_in = (a >= 0) ? 0 : 3;
    }
    else{
        t0 = ty_min;
        face_in = (b >= 0) ? 1 : 4;
    }

    if(tz_min > t0){
        t0 = tz_min;
        face_in = (c >= 0) ? 2 : 5;
    }

    //Vind kleinste t (t1)
    if(tx_max < ty_max){
        t1 = tx_max;
        face_out = (a >= 0) ? 3 : 0;
    }
    else{
        t1 = ty_max;
        face_out = (b >= 0) ? 4 : 1;
    }
    if(tz_max < t1){
        t1 = tz_max;
        face_out = (c >= 0) ? 5 : 2;
    }

    Vector normal{};
    double t = tMin;

    if(t0 < t1 && t1 > kEpsilon){//Bepaal of de ray de box raakt
        if(t0 > kEpsilon){
            t = t0; //hit buiten het oppervlak
            normal = calculateNormal(face_in);
        }
        else{
            t = t1;
            normal = calculateNormal(face_out);
        }

        if(t > tMin) return false; // intersectie ligt verder dan tmin
        intersection = new Intersection(t, normal, *ray);
        return true;
    }

    return false;
}
