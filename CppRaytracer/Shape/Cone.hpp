//
// Created by arnor on 22/12/2020.
//

#ifndef CPPRAYTRACER_CONE_HPP
#define CPPRAYTRACER_CONE_HPP


#include "Shape.hpp"

class Cone: public Shape {
private:
    double radius;
    double height;
    Vector calculateNormal(Point intersection_p);
public:
    Cone(Transformation t, double r, double h): Shape(t), radius{r}, height{h} {};
    ~Cone() override = default;

    bool intersect(Ray* r, double tMin, Intersection*& intersection) override;
};


#endif //CPPRAYTRACER_CONE_HPP
