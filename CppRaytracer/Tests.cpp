//
// Created by Arno Rondou on 16/12/2020.
//

#include "Toolbox/Vector.hpp"
#include <iostream>

void printResult(double result) {
    std::cout << "got: " << result << std::endl;
}
void printResult(Vector result) {
    std::cout << "got: X=" << result.x() << " Y=" << result.y() << " Z=" << result.z() << std::endl;
}
void printResult(Point result) {
    std::cout << "got: X=" << result.x() << " Y=" << result.y() << " Z=" << result.z() << std::endl;
}


void test_Point() {
    std::cout << "Testing POINT" << std::endl;
    std::cout << "Creating Point A: 1,2,3 and Vector B: 4,5,6 and scalar s=2" << std::endl;
    Point a(1, 2, 3); // Automatic storage duration (dead when function ends!)
    Point* b = new Point(4, 5, 6); // On heap (free object store -> NEEDS TO BE DESTRUCTED!!)
    Vector b_vec(4, 5, 6);
    double s = 2;

    std::cout << "test add (A+B) && toVector (B):" << std::endl;
    Point result = a + b_vec;
    printResult(result);

    std::cout << "test subtract (A-B):" << std::endl;
    Vector result_vector = a - *b;
    printResult(result_vector);
}

void test_Vector() {
    std::cout << "Testing Vector" << std::endl;
    std::cout << "Creating Vectors A: 1,2,3 and B: 4,5,6 and scalar s=2" << std::endl;
    Vector a = Vector(1.0, 2.0, 3.0);
    Vector b = Vector(4, 5, 6);
    double s = 2;

    std::cout << "test add (A+B):" << std::endl;
    Vector result = a + b;
    printResult(result);

    std::cout << "test subtract (A-B):" << std::endl;
    result = a - b;
    printResult(result);

    std::cout << "test cross (AxB):" << std::endl;
    result = a.cross(b);
    printResult(result);

    std::cout << "test scale (A*s):" << std::endl;
    result = a * s;
    printResult(result);

    std::cout << "test divide (A/s):" << std::endl;
    result = a / s;
    printResult(result);

    std::cout << "test dot (A*B):" << std::endl;
    std::cout << "TODO" << std::endl;
    double result_d = 0;

    std::cout << "test length (|A|) & lengthsquarred:" << std::endl;
    result_d = a.lengthSquared();
    result_d = sqrt(result_d);
    printResult(result_d);
}

//TODO: Tests for all Toolbox classes