﻿#include <iostream>
#include <cmath>
#include <vector>
#include <random>
#include <chrono>
#include <pthread.h>

#include "Toolbox/toolbox.hpp"
#include "Toolbox/OrthonormalBasis.hpp"
#include "Toolbox/Ray.hpp"
#include "Toolbox/Color.hpp"
#include "Toolbox/Intersection.hpp"
#include "Toolbox/Transformation.hpp"
#include "Scene/PerspectiveCamera.hpp"
#include "Shape/Sphere.hpp"
#include "Shape/Quad.hpp"
#include "Shape/Triangle.hpp"
#include "Shape/Cone.hpp"
#include "Shape/AxisAlignedBox.hpp"
#include "Shading/ColorNode.hpp"
#include "Toolbox/lodepng.h"
#include "Toolbox/ImageBuffer.hpp"
#include "Shading/DiffuseNode.hpp"
#include "Shape/PointShape.hpp"

#define LOGICAL_PROCESSORS 16

using namespace std;

int WIDTH = 1920;
int HEIGHT = 1080;
int ANTI_ALIASING= 4; // Amount of rays there is send per pixel
enum aa_types {
    same, // Generates the rays on equal distances on the pixel, does not solve aliasing.
    only_random, // Generates the rays randomly over the pixel.
    jittered, // Divides the pixel up into ANTI_ALIASING amount squares, then for each square generates a random ray in that square.
    disabeled, // Disables AA only one ray is generated per pixel and value of ANTI_ALIASING is ignored;
} AA_TYPE = jittered;
double SENSITIVITY = 1;
double GAMMA = 2.2;

string output_file_name = "test.png";

static size_t MAX_RAYDISTANCE = 100000000;

/**
 * Progress bar printer, code from: https://stackoverflow.com/a/20528628/11143902
 * @param x value in [0,100] representing the progress.
 * @return sting to be printed
 */
string printProg(int x){
    string s;
    s="[";
    for (int i=1;i<=(100/2);i++){
        if (i<(x/2) || x==100)
            s+="=";
        else if (i==(x/2))
            s+=">";
        else
            s+=" ";
    }

    s+="]";
    return s;
}


Scene* setup(){

    // Check AA requirement
    if (AA_TYPE == same || AA_TYPE == jittered) {
        int n = (int) sqrt((double) ANTI_ALIASING);
        double intpart;
        if (modf(sqrt((double) ANTI_ALIASING), &intpart) != 0.0) {
            ANTI_ALIASING = n * n;
            cout << "For using 'same' AA option the amount of samples per pixel should be perfectly square.\n"
                    "So ANTI_ALIASING = n x n, here n should be a integer to ensure this ANTI_ALIASING of "
                 << ANTI_ALIASING << "is chosen";
        }
    }else if(AA_TYPE == disabeled) ANTI_ALIASING = 1;

    // Create main Camera
    auto* main_camera = new PerspectiveCamera(WIDTH, HEIGHT, new Point(0, 0, 0),
                                              Vector(0, 0, -1),
                                              Vector(0, 1, 0), 90);

    // Create scene
    auto* result = new Scene(main_camera);
    result->maximumRaydistance = MAX_RAYDISTANCE;

    // Create sphere Entity
    auto* s = new Sphere(Transformation::translate(3,0,-8).append(Transformation::scale(1,1,1)));
    auto* c = new ColorNode(Color(1,0,0));
    auto* d = new DiffuseNode(c, 0.8);

    auto* e = new Entity(s, d);
    result->add_entity(e);

    auto* aab  = new AxisAlignedBox(Point(-3,0,-8), 2, 3, 2);
    c = new ColorNode(Color(0,1,0));
    d = new DiffuseNode(c, 0.8);
    e = new Entity(aab, d);
    result->add_entity(e);

    aab = new AxisAlignedBox(Point(2,4,-10), Point(-2,2,-8));
    c = new ColorNode(Color(0,0,1));
    d = new DiffuseNode(c, 0.8);
    e = new Entity(aab, d);
    result->add_entity(e);

    auto* t = new Triangle(Transformation::translate(0,-3,-8).append(Transformation::rotateX(-30)),
                          {-0.5,0.5,0},
                          {-0.5,-0.5,0},
                          {0.5,-0.5,0});
    c = new ColorNode(Color(1,1,0));
    d = new DiffuseNode(c, 0.8);
    e = new Entity(t, d);
    result->add_entity(e);

    auto* q = new Quad(Transformation::translate(-3,1,-8).append(Transformation::rotateX(-30).append(Transformation
            ::scale(2,1,1))));
    c = new ColorNode(Color(1,1,1));
    d = new DiffuseNode(c, 0.8);
    e = new Entity(q, d);
    result->add_entity(e);

    // Create light
    auto* s_l = new PointShape(Transformation::translate(0,18,20));
    auto* l = new Light(s_l,Color(300,300,300));
    result->add_entity(l);


    return result;
}

void saveImageBuffer(string filename, ImageBuffer* imageBuffer){
    vector<unsigned char> png;
    unsigned error = lodepng::encode(png, imageBuffer->getRawImageBuffer(), WIDTH, HEIGHT);
    if(! error) lodepng::save_file(png, filename);

    if(error) cout << "ENCODER ERROR: " << error << " : " <<lodepng_error_text(error) << endl;
}

vector<vector<double>> generateSamplePoints(uniform_real_distribution<double>& rDist, mt19937& mt){

    vector<vector<double>> aa_samples(ANTI_ALIASING,vector<double>(2,0.5));
    if(AA_TYPE == same || AA_TYPE == jittered) {
        int n = sqrt(ANTI_ALIASING);
            for(int aa = 0; aa < ANTI_ALIASING; aa++){
                double pX = aa % n;
                double pY = std::floor(aa / n);
                if(AA_TYPE == same) {
                    aa_samples[aa][0] = (2 / (double)ANTI_ALIASING) * pX + 1 / (double)ANTI_ALIASING;
                    aa_samples[aa][1] = (2 / (double)ANTI_ALIASING) * pY + 1 / (double)ANTI_ALIASING;
                }else if(AA_TYPE == jittered){
                    aa_samples[aa][0] = (2 / (double)ANTI_ALIASING) * pX + rDist(mt) * 1/n;
                    aa_samples[aa][1] = (2 / (double)ANTI_ALIASING) * pY + rDist(mt) * 1/n;
                }
            }
        }
    if(AA_TYPE == only_random){
        for (int aa = 0; aa < ANTI_ALIASING; aa++) {
            aa_samples[aa][0] = rDist(mt);
            aa_samples[aa][1] = rDist(mt);
        }
    }if(AA_TYPE == disabeled){
        vector<vector<double>> oneRayPerPixel(1,vector<double>(2,0.5));
        return oneRayPerPixel;
    }
    return aa_samples;
}

struct Tile{
    int xBegin;
    int yBegin;
    int xEnd;
    int yEnd;
};

vector<Tile> subdivideFrame(int preferredHeight, int preferredWidth){
    vector<Tile> result{};
    for(int y = 0; y < HEIGHT; y += preferredHeight){
        for(int x = 0; x < WIDTH; x += preferredWidth){
            int xEnd = min(WIDTH, x + preferredWidth);
            int yEnd = min(HEIGHT, y + preferredHeight);
            result.push_back({ x, y, xEnd, yEnd});
        }
    }
    return result;
}
struct raytrace_thread_data{
    Scene* main_scene;
    ImageBuffer* imageBuffer;
    uniform_real_distribution<double> dist;
    mt19937 mt;
    int threadID;
    Tile tile;
    bool finished = false;
};

void *raytrace_tile(void *threadArgs){
    struct raytrace_thread_data *args;
    args = (struct raytrace_thread_data*) threadArgs;

    // Iterate over all pixels of the Image
    for(size_t y = args->tile.yBegin; y < args->tile.yEnd; y++){
        for(size_t x = args->tile.xBegin; x < args->tile.xEnd; x++){
            // Bootstrap recursive render-equation
            Color color{}; // Init black color
            // Generate aa samples
            vector<vector<double>> aa_samples = generateSamplePoints(args->dist, args->mt);
            for(int aa = 0; aa < ANTI_ALIASING; aa++){
                PerspectiveCamera* camera = args->main_scene->getCamera();
                double aa_x = aa_samples[aa][0];
                double aa_y = aa_samples[aa][1];

                Ray ray = camera->generateRay(x+aa_x, y+aa_y);
                Intersection* intersection = nullptr;
                bool didIntersect = args->main_scene->findIntersection(&ray, intersection);
                if(didIntersect){
                    color = color + intersection->get_entity()->evaluateRenderEquation(intersection, args->main_scene, 0);
                    delete intersection;
                }
            }
            // add color to Pixel
            args->imageBuffer->setPixel(x,y,color.GetPixelValue(SENSITIVITY, GAMMA));
        }
    }
    args->finished = true; // Notify main that this thread has done it's work.
    pthread_exit(NULL);
    return NULL;
}


int main() {
    // Start timer
    auto start = chrono::high_resolution_clock::now();

    Scene* main_scene = setup();
    ImageBuffer* imageBuffer;

    // Create Image buffer
    imageBuffer = new ImageBuffer(WIDTH, HEIGHT);

    random_device rd; // Get random generator without seed
    mt19937 mt(rd()); // Seed mt19937 with rd
    uniform_real_distribution<double> dist(0,1);

    vector<Tile> tiles = subdivideFrame(HEIGHT/LOGICAL_PROCESSORS, WIDTH/LOGICAL_PROCESSORS);
    int tiles_amount = tiles.size();
    pthread_t threads[tiles_amount];
    pthread_attr_t attr;
    void*status;
    struct raytrace_thread_data tArgs[tiles.size()];
    int rc;

    // Initialize and set thread joinable
    //pthread_attr_init(&attr);
    //pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    int i = 0;
    for(Tile tile: tiles){
        tArgs[i].threadID = i;
        tArgs[i].tile = tile;
        tArgs[i].dist = dist;
        tArgs[i].mt = mt;
        tArgs[i].imageBuffer = imageBuffer;
        tArgs[i].main_scene = main_scene;


        rc = pthread_create(&threads[i], NULL, raytrace_tile, (void *) &tArgs[i]);
        i++;
        if(rc){
            cout << "ERROR: unable to create thread. \nCLOSING, NO IMAGE PRODUCED.\n"<< rc << endl;
            exit(-1);
        }
    }

    bool all_threads_finished = false;

    // check all threads and report the progress
    int prev_progress = -1;
    while (!all_threads_finished){
        int threads_finished = 0;
        for(raytrace_thread_data tData : tArgs) if(tData.finished) threads_finished++;
        if(threads_finished > tiles_amount-1) all_threads_finished = true;
        int progress = ((double)threads_finished/((double)tiles_amount-1)) * 100;
        if(prev_progress != progress){
            cout << "\r" << "Rendering: "<< printProg(progress) << progress << "%" << flush;
            prev_progress = progress;
        }

    }

    cout << endl << "-> Saving Image as:" << output_file_name << endl;

    saveImageBuffer(output_file_name, imageBuffer);

    auto stop = chrono::high_resolution_clock::now();
    auto miliseconds = chrono::duration_cast<chrono::milliseconds>(stop - start);
    int minutes = (int) floor(miliseconds.count()/60000);
    int seconds = (int) floor((miliseconds.count() - (minutes*60000)) / 1000);
    int mili = (int) (miliseconds.count() - (minutes*60000) - (seconds*1000));

    cout << "DONE; This took " << minutes << " minutes " << seconds << "." << mili <<" seconds ";
    return 0;
}

