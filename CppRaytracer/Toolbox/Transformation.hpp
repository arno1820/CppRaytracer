#pragma once
#include "Matrix.hpp"
#include "Vector.hpp"
#include "Ray.hpp"
#include "toolbox.hpp"

class Transformation
{
public:
    Transformation() = default;
    Transformation(Matrix m, Matrix inv) : matrix{ m }, inverse{ inv } {};
    Matrix getTransformationMatrix() const { return Matrix(matrix); };
    Matrix getInverseTransformationMatrix() const { return Matrix(inverse); };
    Transformation invert() const { return Transformation(inverse, matrix); };
    Transformation append(const Transformation& transformation);
    Point transform(Point p) const;
    Point transformInverse(Point p) const;
    Vector transform(Vector v) const;
    Vector transformInverse(Vector v) const;
    Ray transform(Ray r) const;
    Ray transformInverse(Ray r) const;
    static Transformation translate(double x, double y, double z) ;
    static Transformation scale(double x, double y, double z) ;
    static Transformation rotateX(double angle);
    static Transformation rotateY(double angle);
    static Transformation rotateZ(double angle);
    static Transformation rotate(Vector toRotateAround, double angle);
    Vector transformNormal(Vector normal) const;
private:
    Matrix matrix = Matrix(IDENTITY);
    Matrix inverse = Matrix(IDENTITY);
};
