#pragma once
#include "Point.hpp"
#include "Vector.hpp"

/**
 * Represents an ray in three dimensional space starting at a given point and
 * extending infinitely in a given direction.
 * 
 **/

class Point;
class Ray
{
private:
    Point origin;
    Vector direction;
public:
    Ray(const Point o, const Vector d) : origin(o), direction(d) {};
    Ray(const Ray &r) : origin(r.get_origin()), direction(r.get_direction()) {};
    Point get_origin() const { return Point(origin); }
    Vector get_direction() const { return Vector(direction); }
};
