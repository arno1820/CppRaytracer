#include "Transformation.hpp"

Transformation Transformation::append(const Transformation& transformation)
{
	return {matrix * transformation.getTransformationMatrix(), transformation.getInverseTransformationMatrix() * inverse};
}

Point Transformation::transform(Point p) const
{
	return matrix.transform(p);
}

Point Transformation::transformInverse(Point p) const
{
	return inverse.transform(p);
}

Vector Transformation::transform(Vector v) const
{
	return matrix.transform(v);
}

Vector Transformation::transformInverse(Vector v) const
{
	return inverse.transform(v);
}

Ray Transformation::transform(Ray r) const
{
	Point origin = transform(r.get_origin());
	Vector dir = transform(r.get_direction());
	return Ray(origin, dir);
}

Ray Transformation::transformInverse(Ray r) const
{
	Point origin = transformInverse(r.get_origin());
	Vector dir = transformInverse(r.get_direction());
	return Ray(origin, dir);
}

Transformation Transformation::translate(double x, double y, double z) {
	double transformation[4][4] = {1,0,0,x,
									0,1,0,y,
									0,0,1,z,
									0,0,0,1};
	double inverse[4][4] = { 1,0,0,-x,
							0,1,0,-y,
							0,0,1,-z,
							0,0,0,1 };
	return Transformation(Matrix(transformation), Matrix(inverse));
}
Transformation Transformation::scale(double x, double y, double z) {
	double transformation[4][4] = { x,0,0,0,
									0,y,0,0,
									0,0,z,0,
									0,0,0,1 };
	double inverse[4][4] =	{	1/x,0,0,0,
								0,1/y,0,0,
								0,0,1/z,0,
								0,0,0,1 };
	return Transformation(Matrix(transformation), Matrix(inverse));
}
Transformation Transformation::rotateX(double angle) {
	double rad = toRads(angle);
	double s = std::sin(rad);
	double c = std::cos(rad);
	double transformation[4][4] = { 1,0,0,0,
								0,c,-s,0,
								0,s,c,0,
								0,0,0,1 };
	Matrix trans = Matrix(transformation);
	Matrix inverse = trans.transpose();
	return Transformation(trans, inverse);
}
Transformation Transformation::rotateY(double angle) {
	double rad = toRads(angle);
	double s = std::sin(rad);
	double c = std::cos(rad);
	double transformation[4][4] = { c,0,s,0,
									0,1,0,0,
									-s,0,c,0,
									0,0,0,1 };
	Matrix trans = Matrix(transformation);
	Matrix inverse = trans.transpose();
	return Transformation(trans, inverse);
}
Transformation Transformation::rotateZ(double angle) {
	double rad = toRads(angle);
	double s = std::sin(rad);
	double c = std::cos(rad);
	double transformation[4][4] = { c,-s,0,0,
									s,c,0,0,
									0,0,1,0,
									0,0,0,1 };
	Matrix trans = Matrix(transformation);
	Matrix inverse = trans.transpose();
	return Transformation(trans, inverse);
}
Transformation Transformation::rotate(Vector toRotateAround, double angle) {
	double length = toRotateAround.length();
	Vector n = toRotateAround / length;
	double rad = toRads(angle);
	double s = std::sin(rad);
	double c = std::cos(rad);
	double ncos = 1.0 - c;

	double transformation[4][4] = { n.x() * n.x() * ncos + c, n.y() * n.x() * ncos - n.z() * s, n.z() * n.x() * ncos + n.y() * s, 0,
									n.x() * n.y() * ncos + n.z() * s, n.y() * n.y() * ncos + c, n.z() * n.y() * ncos - n.x() * s, 0,
									n.x() * n.z() * ncos - n.y() * s, n.y() * n.z() * ncos + n.x() * s, n.z() * n.z() * ncos + c, 0,
									0, 0, 0, 1 };
	Matrix trans = Matrix(transformation);
	Matrix inverse = trans.transpose();
	return Transformation(trans, inverse);
}
Vector Transformation::transformNormal(Vector normal) const {
	return this->getInverseTransformationMatrix().transpose().transform(normal);
}