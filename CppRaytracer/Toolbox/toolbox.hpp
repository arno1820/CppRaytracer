#pragma once
#include "Matrix.hpp"



constexpr double pi = 3.14159265358979323846;
static double toDegrees(double rads);
double toRads(double degrees);
int SolveQuadric(double c[3], double s[2]);
int SolveCubic(double c[4], double s[3]);
int SolveQuartic(double c[5], double s[4]);
static double ID[4][4] = { 1,0,0,0,
                    0,1,0,0,
                    0,0,1,0,
                    0,0,0,1 };
static const Matrix IDENTITY = Matrix(ID); // Where does this get stored? On the heap?
