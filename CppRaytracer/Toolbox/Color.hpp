#pragma once
#include <cmath>
#include <cstdint>
#include "ImageBuffer.hpp"
class Color
{
public:
    Color() = default;
    Color(double r, double g, double b) : red{ r }, green{ g }, blue{ b }{};
    Color(const Color& c) : red{c.getRed()}, green{c.getGreen()}, blue{c.getBlue()} {};
    double getRed() const { return red; };
    double getGreen() const { return green; };
    double getBlue() const { return blue; };
    Color clamp(double low, double high);
    Color pow(double power);
    Color operator*(const Color& c);
    Color operator*(double d);
    Color operator/(const double d);
    Color operator+(const Color& c);
    Color operator-(const Color& c);

    /** Converts this Color object to a Color ready to be used as pixel values.
    *
    * The contents of the color have radiance as unit,
    * which can have arbitrarily large values. To display these radiance values
    * on a display, they have to be <i>tone-mapped</i> within to values within
    * the range [0, 255].
    *
    * The radiance values are first clamped between zero and the inverse of the
    * given sensitivity. The radiance values are then divided by the given
    * sensitivity in order for all values to be within the range of [0,1].
    * Gamma correction is applied to these values before being multiplied and
    * rounded to the nearest integer in the range [0, 255].
    *
    * @param sensitivity
    *            the sensitivity value to apply to the radiance values.
     *           The radiance values are first clamped to this inverse
     *           of the given sensitivity before being divided by the sensitivity.
     *           Therefore, higher sensitivity means a higher contribution of the
     *           lower radiance values, but results in high radiance values being clamped.
    * @param gamma
    *            the gamma correction to apply.
    **/
    pixel_t GetPixelValue(double sensitivity, double gamma){
        if (sensitivity <= 0)
            throw "the sensitivity must be larger than zero!";
        if (gamma <= 0)
            throw "the gamma must be larger than zero!";

        double invSensitivity = 1.0 / sensitivity;
        double invGamma = 1.0 / gamma;

        pixel_t pixel = ((clamp(0, invSensitivity) * sensitivity).pow(invGamma) * 255).toRGB();
        return pixel;
    }
private:
    pixel_t toRGB() const {
        uint8_t r = fmin(255, fmax(0, round(red)));
        uint8_t g = fmin(255, fmax(0, round(green)));
        uint8_t b = fmin(255, fmax(0, round(blue)));
        return {r,g,b};
    }
    double red = 0;
    double green = 0;
    double blue = 0;
};
