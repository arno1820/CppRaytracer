#include "Color.hpp"
#include <cmath>
//TODO: Implementation


Color Color::clamp(double low, double high) {
    double r = std::min(high, std::max(low, red));
    double g = std::min(high, std::max(low, green));
    double b = std::min(high, std::max(low, blue));

    return Color(r, g, b);
}

Color Color::pow(double power) {
    return Color(std::pow(red, power), std::pow(green, power), std::pow(blue, power));
}

Color Color::operator*(const Color &c) {
    return Color(red*c.red, green*c.green, blue*c.blue);
}

Color Color::operator*(const double d) {
    return Color(red*d, green*d, blue*d);
}

Color Color::operator/(const double d) {
    if(d == 0) throw "Cannot divide color with zero!";
    return Color(red/d, green/d, blue/d);
}

Color Color::operator+(const Color &c) {
    return Color(red+c.red, green+c.green, blue+c.blue);
}

Color Color::operator-(const Color &c) {
    return Color(red-c.red, green-c.green, blue-c.blue);
}
