#pragma once
#include "Vector.hpp"
#include <iostream>
#include <cmath>

class OrthonormalBasis
{
    private:
        Vector u = Vector(0,0,0);
        Vector v = Vector(0,0,0);
        Vector w = Vector(0,0,0);
public:
    explicit OrthonormalBasis(Vector a){
        double length = a.length();
        if(length == 0) throw "The given vector has length 0 for construction of the orthonormal Basis!";
        w = a / length;
        if(abs(w.x()) > abs(w.y())){
            double inv_length = 1 / sqrt(w.x() * w.x() + w.z() * w.z());
            u = Vector(-w.z() * inv_length, 0, w.x() *inv_length);
        } else {
            double inv_length = 1 / sqrt(w.y() * w.y() + w.z() * w.z());
            u = Vector(0, w.z() * inv_length, -w.y() *inv_length);
        }
        v = w.cross(u);
    };

    OrthonormalBasis(Vector a, Vector b){
        Vector cross = b.cross(a);
        double length = cross.length();
        if(length == 0 || length < 1e-8) throw "CREATING ORTHONORMAL BASIS WITH COLLINEAR VECTORS";
        w = a.normalize();
        u = cross / length;
        v = w.cross(u);
    };
    Vector get_u() const { return Vector(u); };
    Vector get_v() const { return Vector(v); };
    Vector get_w() const { return Vector(w); };
};
