#pragma once
#include "Vector.hpp"

class Vector;
class Point
{
private:
    double x_val = 0;
    double y_val = 0;
    double z_val = 0;
public:
    Point(double xv, double yv, double zv): x_val{ xv }, y_val{ yv }, z_val{ zv } {};
    Point(const Point& p): x_val{ p.x() }, y_val{ p.y() }, z_val{ p.z() } {};
    double x() const {return x_val;};
    double y() const {return y_val;};
    double z() const {return z_val;};
    Point operator+(const Vector& other) const;
    Vector operator-(const Point& other) const;
    Point operator*(double s) const;
    Point operator/(double s) const;
    bool operator==(const Point& other) const;
    Vector toVector();
};
