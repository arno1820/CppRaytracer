#include "Matrix.hpp"

Matrix Matrix::operator+(const Matrix& other){
    double result[4][4];
    for(int i = 0; i<4; i++){
        for(int j = 0; j<4; j++){
            result[i][j] = elements[i][j] + other.get(i,j);
        }
    }
    return Matrix(result);
}
Matrix Matrix::operator-(const Matrix& other){
    double result[4][4];
    for(int i = 0; i<4; i++){
        for(int j = 0; j<4; j++){
            result[i][j] = elements[i][j] - other.get(i,j);
        }
    }
    return Matrix(result);
}
Matrix Matrix::operator*(const double s){
    double result[4][4];
    for(int i = 0; i<4; i++){
        for(int j = 0; j<4; j++){
            result[i][j] = elements[i][j] * s;
        }
    }
    return Matrix(result);
}
Matrix Matrix::operator*(const Matrix& other){
    double result[4][4];
    for(int i = 0; i<4; i++){
        for(int j = 0; j<4; j++){
            double value = 0;
            for(int k = 0; k<4; k++) value += elements[i][k] * other.get(k,j);
            result[i][j] = value;
        }
    }
    return Matrix(result);
}
bool Matrix::operator==(const Matrix& other){
    if(&other == this) return true;
    for(int i = 0; i<4; i++){
        for(int j = 0; j<4; j++){
            if(elements[i][j] != other.get(i,j)) return false;
        }
    }
    return true;
}
Matrix Matrix::transpose() const{
    double result[4][4];
    for(int i = 0; i<4; i++){
        for(int j = 0; j<4; j++){
            result[i][j] = elements[j][i];
        }
    }
    return Matrix(result);
}
Point Matrix::transform(Point p) const{
    double x = elements[0][0] * p.x() + elements[0][1] * p.y() + elements[0][2] * p.z() + elements[0][3];
    double y = elements[1][0] * p.x() + elements[1][1] * p.y() + elements[1][2] * p.z() + elements[1][3];
    double z = elements[2][0] * p.x() + elements[2][1] * p.y() + elements[2][2] * p.z() + elements[2][3];
    double w = elements[3][0] * p.x() + elements[3][1] * p.y() + elements[3][2] * p.z() + elements[3][3];
    //double invW = 1/w;
    return Point(x / w, y / w, z / w); // Point(x * invW, y * invW, z * invW);
}
Vector Matrix::transform(Vector v) const{
    double x = elements[0][0] * v.x() + elements[0][1] * v.y() + elements[0][2] * v.z();
    double y = elements[1][0] * v.x() + elements[1][1] * v.y() + elements[1][2] * v.z();
    double z = elements[2][0] * v.x() + elements[2][1] * v.y() + elements[2][2] * v.z();
    return Vector(x, y, z);
}