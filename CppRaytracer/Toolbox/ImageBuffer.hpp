//
// Created by arnor on 15/12/2020.
//
#include <vector>
#include <cstdint>

#ifndef CPPRAYTRACER_IMAGEBUFFER_HPP
#define CPPRAYTRACER_IMAGEBUFFER_HPP

typedef struct
{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
}
pixel_t;

class ImageBuffer {
private:
    std::vector<unsigned char> pixels;
    size_t width;
    size_t height;
public:
    ImageBuffer( size_t w, size_t h ): width{w}, height{h} {
        pixels.resize(w * h * 4);
    }
    /**
     *
     * @return The raw ImageBuffer (for saving for example) ! THE RAW BUFFER USES TOP TO BOTTOM AND LEFT TO RIGHT !
     * ! THIS MEANS THE LOWER PART OF THE SCREEN IS +Y AND RIGHT PART IS +X !
     */
    std::vector<unsigned char> getRawImageBuffer(){
        return pixels;
    }
    pixel_t getPixel(size_t x, size_t y) const{
        y = height-y-1; // The png writer expects top to bottom, so y-axis is positive at the bottom of the screen,
                        // whilst this raytracer goes bottom to top (based on used literature).
        uint8_t r = pixels[x*4 + y*width*4 + 0];
        uint8_t g = pixels[x*4 + y*width*4 + 1];
        uint8_t b = pixels[x*4 + y*width*4 + 2];

        return {r,g,b};
    }
    size_t getHeight() const{
        return height;
    }
    size_t getWidth() const{
        return width;
    }

    void setPixel(size_t x, size_t y, pixel_t p){
        y = height-y-1; // The png writer expects top to bottom, so y-axis is positive at the bottom of the screen,
                        // whilst this raytracer goes bottom to top (based on used literature).
        pixels[x*4 + y*width*4 + 0] = p.red;
        pixels[x*4 + y*width*4 + 1] = p.green;
        pixels[x*4 + y*width*4 + 2] = p.blue;
        pixels[x*4 + y*width*4 + 3] = 255; // A value is always max
    }

};


#endif //CPPRAYTRACER_IMAGEBUFFER_HPP
