#include "Point.hpp"

Point Point::operator+(const Vector& other) const{return Point{x_val + other.x(), y_val + other.y(), z_val + other.z()};};
Vector Point::operator-(const Point& other) const{return Vector{x_val - other.x_val, y_val - other.y_val, z_val - other.z_val};};
Point Point::operator*(double s) const{return Point{x_val * s, y_val * s, z_val * s};};
Point Point::operator/(double s) const{return Point{x_val / s, y_val / s, z_val / s};};
bool Point::operator==(const Point& other) const{
    if(other.x_val == x_val && other.y_val == y_val && other.z_val == z_val) return true;
    return false;
};
Vector Point::toVector(){
    return Vector(x_val, y_val, z_val);
}
