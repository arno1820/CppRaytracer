#pragma once
#include <cmath>
#include "Point.hpp"
#include "Vector.hpp"

class Point;
class Vector;
class Matrix
{
private:
    double elements[4][4] = {0,0,0,0,
                            0,0,0,0,
                            0,0,0,0,
                            0,0,0,0};

public:
    explicit Matrix(double m[4][4]){
        // Can't this be done better?
        for(int i = 0; i<4; i++){
            for(int j = 0; j<4; j++){
                elements[i][j] = m[i][j];
            }
        }
    };
    Matrix(const Matrix& m) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                elements[i][j] = m.get(i,j);
            }
        }
    };
    double get(const int row, const int col) const{
        return elements[row % 4][col % 4]; // mod 4 so the value stays in the bounds
    };
    void set(const double value, const int row, const int col) {
        elements[row % 4][col % 4] = value;
    };
    Matrix operator+(const Matrix& other);
    Matrix operator-(const Matrix& other);
    Matrix operator*(double s);
    Matrix operator*(const Matrix& other);
    bool operator==(const Matrix& other);
    Matrix transpose() const;
    Point transform(Point p) const;
    Vector transform(Vector v) const;

};
