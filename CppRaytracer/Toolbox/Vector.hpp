#pragma once
#include <cmath>
#include "Point.hpp"

class Point;
class Vector
{
private:
    double x_val = 0;
    double y_val = 0;
    double z_val = 0;
public:
    Vector(double xv, double yv, double zv): x_val{xv}, y_val{yv}, z_val{zv} {};
    Vector(const Vector& v) : x_val{ v.x() }, y_val{ v.y() }, z_val{ v.z() } {};
    Vector() = default;
    double x()const{return x_val;};
    double y()const{return y_val;};
    double z()const{return z_val;};
    Vector operator+(Vector other);
    Vector operator-(Vector other);
    Vector operator*(double s);
    Vector operator/(double s);
    bool operator==(Vector other);
    Point toPoint()const;
    double dot(const Vector& other) const;
    Vector cross(const Vector& other)const;
    double lengthSquared()const;
    double length()const;
    Vector normalize()const;
    Vector invert()const;


};
