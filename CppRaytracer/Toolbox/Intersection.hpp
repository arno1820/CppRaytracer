/**
 * Class, which encapsulates all data of a certain ray intersection
**/
#pragma once
#include "../Toolbox/Ray.hpp"

class Entity;

class Intersection
{
private:
    Point intersection;
    Vector normal;
    double cosAngle;
    Ray ray;
    Entity* entity;
    static double calculateCosAngle(const Ray ray, const Vector normal) {
        return (ray.get_direction() * -1).dot(normal) / (normal.length() * ray.get_direction().length());
    };
public:
    Intersection(double t, Vector n, Ray r) : intersection(r.get_origin() + (r.get_direction().normalize() * t)),
                                                        normal(n), ray(r), cosAngle(calculateCosAngle(r, n)) {};
    Point get_intersection() const { return Point(intersection); };
    Vector get_normal() const { return Vector(normal); };
    double get_cosAngle() const { return cosAngle; };
    Ray get_ray() const { return Ray(ray); };
    double getT() {return (intersection - ray.get_origin()).length();};
    Entity* get_entity(){
        return entity;
    }
    void set_entity(Entity* e) {entity = e;};
};
    
