#include "Vector.hpp"

Vector Vector::operator+(Vector other){return Vector{x_val + other.x(), y_val + other.y(), z_val + other.z()};};
Vector Vector::operator-(Vector other){return Vector{x_val - other.x_val, y_val - other.y_val, z_val - other.z_val};};
Vector Vector::operator*(const double s){return Vector{x_val * s, y_val * s, z_val * s};};
Vector Vector::operator/(const double s){return Vector{x_val / s, y_val / s, z_val / s};};
bool Vector::operator==(Vector other){
    if(other.x_val == x_val && other.y_val == y_val && other.z_val == z_val) return true;
    return false;
};
Point Vector::toPoint() const{
    return Point(x_val, y_val, z_val);
};
double Vector::dot(const Vector& other) const{
    return x_val * other.x_val + y_val * other.y_val + z_val * other.z_val;
};
Vector Vector::cross(const Vector& other) const{
    double xx = y_val * other.z_val - z_val * other.y_val;
    double yy = z_val * other.x_val - x_val * other.z_val;
    double zz = x_val * other.y_val - y_val * other.x_val;
    return Vector(xx, yy, zz);
};
double Vector::lengthSquared() const{
    return x_val * x_val + y_val * y_val + z_val * z_val;
};
double Vector::length() const{
    return sqrt(lengthSquared());
};
Vector Vector::normalize() const{
    return Vector(*this) / this->length();
};
Vector Vector::invert() const{
    return Vector(-x_val, -y_val, -z_val);
}
