//
// Created by arnor on 4/12/2020.
//

#ifndef CPPRAYTRACER_SHADERPIPELINE_H
#define CPPRAYTRACER_SHADERPIPELINE_H
#include "../Toolbox/Color.hpp"
#include "../Toolbox/Vector.hpp"
#include <vector>
class Intersection;

class Scene;
class Entity;
class Point;


/**
 * Is an abstract class for a node in the shader Pipeline, these nodes calculate the outgoing radiance(Color) of the object hit by a ray.
 */
class ShaderNode {
protected:
     /**
     * @param e the entity for who this node is calculating the color.
     * @param normal the normal of the outgoing intersection (to viewer).
     * @param p the point in space from what to send the shadowrays.
     * @param s the scene where it takes place.
     * @return a list of all direct light object intersections, for pointlights this is only one per light for area lights
    * this can be more.
     */
    static std::vector<Intersection> getDirectLighting(const Entity *e, const Vector *normal, const Point *p, const Scene *s) ;
public:
    virtual Color eval(Intersection* i, Scene* s, double depth, const Entity* e) const = 0;
};

#endif //CPPRAYTRACER_SHADERPIPELINE_H
