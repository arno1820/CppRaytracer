//
// Created by arnor on 18/12/2020.
//

#include "ShaderNode.hpp"
#include "../Scene/Scene.hpp"
#include "../Shape/Shape.hpp"

std::vector<Intersection> ShaderNode::getDirectLighting(const Entity* e, const Vector* normal, const Point *p, const Scene *s) {
    std::vector<Intersection> result;
    std::vector<Light*> lights = s->getLights();
    for(Light* l: lights){
        // first get points of the light (pointlight will only return one)
        std::vector<Point> points = l->getPointsOnLight();
        for(Point p_light : points){
            Intersection* to_add_intersection = nullptr;
            bool visible = s->testVisibility(e, p, l, &p_light, normal, to_add_intersection);
            if(visible) result.push_back(*to_add_intersection);
        }
    }
    return result;
}
