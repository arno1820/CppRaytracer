//
// Created by arnor on 18/12/2020.
//

#ifndef CPPRAYTRACER_DIFFUSENODE_HPP
#define CPPRAYTRACER_DIFFUSENODE_HPP


#include "ShaderNode.hpp"

class DiffuseNode : public ShaderNode {
private:
    ShaderNode* prevNode;
    double kdFactor;
public:
    DiffuseNode(ShaderNode* cN, double kd): prevNode{cN}, kdFactor{kd} {};
    Color eval(Intersection* i, Scene* s, double depth, const Entity* e) const override;
};


#endif //CPPRAYTRACER_DIFFUSENODE_HPP
