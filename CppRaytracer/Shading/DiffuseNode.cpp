//
// Created by arnor on 18/12/2020.
//

#include "DiffuseNode.hpp"
#include "../Scene/Scene.hpp"
#include <vector>
#include "ColorNode.hpp"
#include "../Shape/Shape.hpp"
Color DiffuseNode::eval(Intersection *i, Scene *s, double depth, const Entity *e) const {
    Color result{};
    Color cN = prevNode->eval(i,s,depth,e);
    const Vector normal = i->get_normal();
    const Point intersectPoint = i->get_intersection();
    std::vector<Intersection> directLightIntersections = getDirectLighting(e,&normal,&intersectPoint, s);



    for(Intersection light_int : directLightIntersections){

        // find cos(theta)
        Vector toLight = light_int.get_entity()->getShape()->getPosition() - i->get_intersection();
        double cosAngle = toLight.dot(normal) / (normal.length() * toLight.length());

        Color light_color = light_int.get_entity()->evaluateRenderEquation(&light_int, s, depth);

        double radius_squared = toLight.lengthSquared();

        Color E = light_color * cN * (cosAngle/(radius_squared * 4 * M_PI));

        result =  result + E * (kdFactor/M_PI);
    }
    return result;
}
