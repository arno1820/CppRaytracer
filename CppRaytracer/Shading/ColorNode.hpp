//
// Created by arnor on 9/12/2020.
//

#ifndef CPPRAYTRACER_COLORNODE_HPP
#define CPPRAYTRACER_COLORNODE_HPP
#include "ShaderNode.hpp"

/**
 * This is a color node, it static eval function returns the static color set at construction.
 */
class ColorNode : public ShaderNode {
private:
    Color color = Color(0, 0, 0);
public:
    ColorNode() = default;

    explicit ColorNode(const Color& c): color{c} {};

    Color eval(Intersection* i, Scene* s, double depth, const Entity* e) const;

};


#endif //CPPRAYTRACER_COLORNODE_HPP
